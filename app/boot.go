package app

import (
	"net/http"
	"os"

	"github.com/BurntSushi/toml"

	jsoniter "github.com/json-iterator/go"
	log "github.com/sirupsen/logrus"
)

type tomlConfig struct {
	Title    string
	Server   configuration
	Database database
}

type configuration struct {
	Name         string
	Version      string
	Address      string
	Port         int
	Tags         []string
	ReadTimeout  int
	WriteTimeout int
}

type database struct {
	Username string
	Password string
	Port     string
	Host     string
	DB       string
	SSLMode  string
}

type errorMessage struct {
	Code    int    `json:"code"`
	Error   string `json:"error"`
	Message string `json:"message"`
}

type appError struct {
	Error      string `json:"error"`
	Message    string `json:"message"`
	HttpStatus int    `json:"status"`
}

type errorResource struct {
	Data errorMessage `json:"status"`
}

// AppConfig holds the configuration values from config.toml file
var AppConfig tomlConfig

// Initialize AppConfig
func init() {
	//load config
	loadAppConfig()

	//set log format and level
	if os.Getenv("PLATFORM") == "Production" {
		log.SetLevel(log.InfoLevel)
	} else if os.Getenv("PLATFORM") == "Staging" {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.DebugLevel)
	}
	log.SetFormatter(new(COCOJSONFormatter))
}

// Reads config.toml and decode into AppConfig
func loadAppConfig() {
	if _, err := toml.DecodeFile("app/config/config.toml", &AppConfig); err != nil {
		log.Error(err)
		return
	}
}

// DisplayAppError - Display error on err != nil
func DisplayAppError(w http.ResponseWriter, handlerError error, message string, code int) {
	errObj := errorMessage{
		Error:   handlerError.Error(),
		Code:    code,
		Message: message,
	}

	log.Error("Error processing request")
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	if j, err := jsoniter.Marshal(errorResource{Data: errObj}); err == nil {
		w.Write(j)
	}
}
