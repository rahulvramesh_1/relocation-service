package controllers

import (
	"fmt"
	"net/http"

	relocationUsecase "bitbucket.org/evhivetech/relocation-service/usecase"
)

type HttpRelocationHandler struct {
	RelocationUsecase relocationUsecase.RelocationUsecase
}

func NewRelocationHandler(
	us relocationUsecase.RelocationUsecase,
) *HttpRelocationHandler {
	return &HttpRelocationHandler{
		RelocationUsecase: us,
	}
}

func (handler *HttpRelocationHandler) Add(w http.ResponseWriter, r *http.Request) {
	fmt.Println("called")
	w.WriteHeader(200)
	w.Write([]byte{1})
}
