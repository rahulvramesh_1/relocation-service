package helper

import "time"

type TimeHandler interface {
	BeginningOfMonth() time.Time
	EndOfMonth() time.Time
}

// Now now struct
type Now struct {
	t time.Time
}

// New initialize Now with time
func New(t time.Time) *Now {
	return &Now{t}
}

// BeginningOfMonth beginning of month
func (now *Now) BeginningOfMonth() time.Time {
	y, m, _ := now.t.Date()
	return time.Date(y, m, 1, 0, 0, 0, 0, now.t.Location())
}

// EndOfMonth end of month
func (now *Now) EndOfMonth() time.Time {
	return now.BeginningOfMonth().AddDate(0, 1, 0).Add(-time.Nanosecond)
}

// Golang unzip from timezone fail
func GetNowTimeGMT() time.Time {
	loc, err := GetLoc()
	if err != nil {
		return time.Now().Add(time.Hour * time.Duration(7)) // Add 7 hours from local time
	}

	t := time.Now().In(loc)
	return t
}

// Time Location GMT
func GetLoc() (loc *time.Location, err error) {
	// 1st Try Indonesia
	loc, err = time.LoadLocation("Asia/Jakarta")
	if err != nil {
		// 2nd Try Thailand
		loc, err = time.LoadLocation("Asia/Bangkok")
		if err != nil {
			// 3rd Try Vietnam
			loc, err = time.LoadLocation("Asia/Saigon")
		}
	}

	return loc, err
}
