package postgres

import (
	"fmt"

	relocationRepository "bitbucket.org/evhivetech/relocation-service/repository"
	"github.com/jinzhu/gorm"
)

type postgresRelocationRepository struct {
	DB *gorm.DB
}

func NewPostgresRelocationRepository(db *gorm.DB) relocationRepository.RelocationRepository {

	return &postgresRelocationRepository{
		DB: db,
	}
}

func (p *postgresRelocationRepository) TestFun() error {
	fmt.Println("test")

	return nil
}
