package cases

import (
	"fmt"

	relocationRepo "bitbucket.org/evhivetech/relocation-service/repository"
	_relocationUcase "bitbucket.org/evhivetech/relocation-service/usecase"
)

type articleUsecase struct {
	relocationRepo relocationRepo.RelocationRepository
}

func NewRelocationUsecase(
	rr relocationRepo.RelocationRepository,
) _relocationUcase.RelocationUsecase {
	return &articleUsecase{
		relocationRepo: rr,
	}
}

func (a *articleUsecase) TestFun() error {
	fmt.Println("test repo")

	return nil
}
